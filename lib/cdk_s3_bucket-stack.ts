import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3deploy from 'aws-cdk-lib/aws-s3-deployment';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class CdkS3BucketStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const bucket = new cdk.aws_s3.Bucket(this, 'XiuBucket', {
      versioned: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED,
      enforceSSL: true,
      autoDeleteObjects: true
    });

    new s3deploy.BucketDeployment(this, 'object1', {
      sources: [s3deploy.Source.asset('./web')],
      destinationBucket: bucket,
      destinationKeyPrefix: 'web',
      retainOnDelete: false,
      prune: false,
      cacheControl: [s3deploy.CacheControl.setPublic(), s3deploy.CacheControl.maxAge(cdk.Duration.days(365))],
      contentLanguage: 'zh-CN',
      metadata: {
        author: 'xiu',
      }
    })

    new cdk.CfnOutput(this, 'XiuBucketCdk', { value: bucket.bucketName });
  }
}
