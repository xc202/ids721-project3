## S3 Bucket using CDK with AWS CodeWhisperer

I installed the **Node.js** from official website and used `cdk init app --language=typescript` to initialize a new AWS Cloud Development Kit (CDK) application project in TypeScript.

### 1. Project File Structure

The first part is about the overall project file structure:

```markdown
.
├── bin
│   └── cdk-s3-bucket.ts
├── lib
│   └── cdk-s3-bucket-stack.ts
├── node_modules
├── screenshot
├── test
│   └── cdk-s3-bucket.test.ts
├── web
│   └── upload.json
├── cdk.json
├── jest.config.js
├── package.json
├── package-lock.json
├── README.md
├── tsconfig.json
└── yarn.lock
```

* `bin/cdk-s3-bucket.ts`: Contains the entry point for the CDK app.
* `lib/cdk-s3-bucket-stack.ts`: Contains the definition of the CDK stack.
* `node_modules`: Contains project dependencies.
* `screenshot`: Contains figures of results. 
* `test`: Contains files for testing.
* `web`: Folder ready be uploaded to the AWS bucket.
* `cdk.json`: Configuration file for the CDK app.
* `jest.config.js`: Configuration file for the Jest testing framework.
* `package.json`: Node.js package configuration file.
* `package-lock.json`: Locks down the versions of dependencies in a project.
* `README.md`: Project documentation file.
* `tsconfig.json`: TypeScript configuration file.
* `yarn.lock`: Yarn package manager lock file.

#### Start Process
To begin with, you need first run `brew install awscli` to install AWS CLI and `aws configure` to configure your AWS account, and run `cdk bootstrap` to set up and configure the necessary resources, such as an Amazon S3 bucket, in your AWS account for the AWS Cloud Development Kit (CDK) Toolkit. This enables the CDK Toolkit to interact with the AWS Cloud for deploying CDK applications.
Finally, run `cdk deploy` to deploy the AWS Cloud Development Kit (CDK) application by creating or updating the corresponding CloudFormation stack in your AWS account. It automates the deployment process based on the infrastructure defined in your CDK app, ensuring the specified resources are provisioned or updated according to your code.

#### CI/CD
Alternatively, you can simply clone the repository and push any changes. `.gitlab-ci.yml` was written for auto deployment. Once the branch `master` in gitlab has been pushed, the system will automatically deploy the bucket to AWS. The default AWS account is my account.

### 2. Code Logic Explanation

The second part explains the specific code logic for implementing the S3 bucket in the `lib/cdk-s3-bucket-stack.ts` file:

* `CdkS3BucketStack` class extends `cdk.Stack` and represents a CloudFormation Stack.
* In the constructor, an S3 bucket (`bucket`) is created with certain configurations such as enabling versioning, using KMS for encryption, etc.
* Using `s3deploy.BucketDeployment`, the contents of a local folder (`./web`) are deployed to the S3 bucket with specified deployment options like destination path, cache control, content language, etc.
* A `cdk.CfnOutput` is used to output the name of the S3 bucket as a CloudFormation output, which can be viewed after deployment.

### 3. Usage of AWS CodeWhisperer

Amazon CodeWhisperer enhances coding productivity by offering real-time code suggestions in the integrated development environment (IDE), spanning from concise code snippets to entire functions, based on your comments and existing code.

In this project, I leveraged AWS CodeWhisperer to automate the majority of the code, streamlining my development process. For instance, it facilitated the automatic completion of default bucket properties and deploy properties, saving time when I inserted line breaks. Regarding the setup, using the IntelliJ IDEA editor, I first downloaded the corresponding plugin from the official website. IntelliJ IDEA automatically detected and integrated the plugin into the environment. To utilize the functionalities of AWS Q and CodeWhisperer, connecting via AWS Builder ID to link with the account was necessary. Following this, coding became more efficient and convenient.

![Screenshot 2024-02-15 at 1.13.53 AM.png](screenshot%2FScreenshot%202024-02-15%20at%201.13.53%20AM.png)

### 4. Outcomes

The final part includes screenshots of the generated S3 bucket, indicating the successful completion of the project. We can see that the bucket `cdks3bucketstack-xiubucketce9ca0fa-fyvyidbvigto` has been created, containing the uploaded `web` folder, which includes the `upload.json` file:

![Screenshot 2024-02-15 at 12.50.59 AM.png](screenshot%2FScreenshot%202024-02-15%20at%2012.50.59%20AM.png)

![Screenshot 2024-02-15 at 12.52.04 AM.png](screenshot%2FScreenshot%202024-02-15%20at%2012.52.04%20AM.png)

![Screenshot 2024-02-15 at 12.52.28 AM.png](screenshot%2FScreenshot%202024-02-15%20at%2012.52.28%20AM.png)
